#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import pandas as pd
import numpy as np
import re
from Utility import *
from PENDFFileReader import *

class PENDFFile_MF3(PENDFFileReader):
	def __init__(self, file_in_path, file_out_path, temperature):
		#print('PENDFFile_MF3: file_in=', file_in_path)
		PENDFFileReader.__init__(self, file_in_path, file_out_path)
		#-------------------------------------------------------------
		# Read and transform PENDF data to G4NDL data
		#-------------------------------------------------------------
		items = self.readLine() #header 1
		self.ZA = items[0]
		self.AWR = items[1]
		self.MF = items[7]
		self.MT = items[8]

		items = self.readLine() #header 2

		self.QM = items[0]
		self.QI = items[1]
		self.LR = items[3]
		self.NR = items[4]
		self.NP = items[5]

		items = self.readLine() #header 3
		energy_xs = []
		items = self.readLine()
		number_pair_data = 0
		
		line_to_write = ''
		while (self.MF==items[7] and self.MT==items[8]):
			data_line = []
			for i in range(6):  #read pair (energy, cross-section)
				if items[i]!=None:
					data_line.append( items[i] ) #, items[2*i+1]] )
					line_to_write = line_to_write + str(format(items[i], '.6e')) + ' '
					
				if i%2==0 and items[i]!=None :
				    number_pair_data += 1
			
			line_to_write = line_to_write + '\n'  	    
			energy_xs.append(data_line)
			items = self.readLine()

		#line_to_write = line_to_write[:-1]
		#-------------------------------------------------------------
		# Write GNDL file
		#-------------------------------------------------------------
		#self.file_out.write(formatString('G4NDL')+'\n')
		#self.file_out.write(formatString(ENDF_LIBRARY)+'\n')
		self.file_out.write(formatString(self.MF)+'\n') 
		self.file_out.write(formatString(self.MT)+'\n') 
		self.file_out.write(formatString(temperature)+'\n') 
		self.file_out.write(formatString(int(number_pair_data))+'\n') #Number of pair of lines to process
		self.file_out.write(line_to_write)
				
		#df = pd.DataFrame(energy_xs, columns=['Energy', 'Cross-section', 'Energy', 'Cross-section', 'Energy', 'Cross-section'])
		#####df.replace(np.nan, 0) #Replace NaN by 0
		#print(df)
		#values = df.values
		#np.savetxt(self.file_out, values, fmt='%.6e')

		#Works
		#values_2 = np.nan_to_num(values, nan=0, posinf=33333333, neginf=33333333)
		#np.savetxt(self.file_out, values_2, fmt='%.6e')

		
		#columns = df.columns.values
		#np.savetxt(self.file_out, values, fmt='%.6e')

		self.file_out.close()
		
        
