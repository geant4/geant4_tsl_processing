#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import pandas as pd
import os

if __name__ == "__main__":
	db_1_path = 'files_relation_all_jeff33_tsl_jeff33.csv'
	db_2_path = 'files_relation_all_jeff33_tsl_endfB8.csv'
	merge_file_path = 'files_relation_all_jeff33_tsl_mix_jeff33-endfB8.csv'
	merge_file_comp_path = 'files_relation_all_jeff33_tsl_mix_jeff33-endfB8_droppedMaterialFromB80.csv'
	
	df_1 = pd.read_csv(db_1_path)
	df_2 = pd.read_csv(db_2_path)
	g4_full_name_list = df_1["Geant4FullName"]
	
	merge_file_comp = open(merge_file_comp_path, 'w')
	merge_file_comp.write('TSL,Neutron,Mtref,Natoms,Temperature,Geant4Name,Geant4FullName\n')
	
	for idx, row in df_2.iterrows():
		is_found = False
		for name in g4_full_name_list:
			name = name.replace(' ', '')
			if row["Geant4FullName"] == name:
				is_found = True
							
		
		if not is_found:	
			entry = df_2.loc[df_2['Geant4FullName'] == row["Geant4FullName"]]
			df_1 = df_1.append( {'TSL':row['TSL'], 'Neutron': row['Neutron'], 'Mtref':row['Mtref'], 'Natoms':row['Natoms'], 'Temperature':row['Temperature'], 'Geant4Name':row['Geant4Name'], 'Geant4FullName':row['Geant4FullName'] } , ignore_index=True)
			
		else:
			item =  {'TSL':row['TSL'], 'Neutron':row['Neutron'], 'Mtref':row['Mtref'], 'Natoms':row['Natoms'], 'Temperature':row['Temperature'], 'Geant4Name':row['Geant4Name'], 'Geant4FullName':row['Geant4FullName'] }
			print('already in bib: ',item)
			merge_file_comp.write(str(row['TSL'])+','+str(row['Neutron'])+','+str(row['Mtref'])+','+str(row['Natoms'])+','+str(row['Temperature'])+','+str(row['Geant4Name'])+','+str(row['Geant4FullName'])+ '\n')
	
	#print(df_1)
	
	df_1.to_csv(merge_file_path, index=False)
	merge_file_comp.close()
	
