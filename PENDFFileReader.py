#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

from Utility import *

DICT_MF = []
DICT_MF.append( [3, 'CrossSection'] )
DICT_MF.append( [6, 'FS'] )


#************* PENDF reader *****************************************
class PENDFFileReader(object):  
    def __init__(self, file_in_path, file_out_path):
        self.file = open(file_in_path, 'r')
        if file_out_path!="dummy":
            self.file_out = open(file_out_path, 'w')
            self.file_out_path = file_out_path
            self.file.readline()
            items = self.readLine()
            self.MF = items[7]
            self.MT = items[8]
            self.file.seek(0)
    
    def getMF_MT(self):
        return MF, MT
           
    def splitLine(self, line):
        items = []
        i_start = 0
        i_step = 11
        for i in range(6):
            items.append( string_to_float(line[i_start:i_start+i_step]) ) #values
            i_start += i_step
        
        items.append( string_to_int(line[i_start:i_start+4]) ) #MAT  
        items.append( string_to_int(line[i_start+4:i_start+6]) ) #MF
        items.append( string_to_int(line[i_start+6:i_start+9]) ) #MT
        #items.append( string_to_int(line[i_start+9:i_start+14]) ) #MF-MT line number
        return items
    
    def splitLine_v2(self, line):
        items = []
        i_start = 0
        i_step = 11
        for i in range(6):
            print('chunk=', line[i_start:i_start+i_step])
            items.append( string_to_float(line[i_start:i_start+i_step]) ) #values
            print('items=', items[i])
            i_start += i_step
        
        items.append( string_to_int(line[i_start:i_start+4]) ) #MAT 
        print('items=', items[6])
        items.append( string_to_int(line[i_start+4:i_start+6]) ) #MF
        print('items=', items[7])
        items.append( string_to_int(line[i_start+6:i_start+9]) ) #MT
        print('items=', items[8])
        #items.append( string_to_int(line[i_start+9:i_start+14]) ) #MF-MT line number
        return items
    
    def readLine(self):
        line = self.file.readline()
        items = self.splitLine(line)
        return items
        
    def readLine_and_getMF_MT(self, line):
        items = self.splitLine(line)
        return items, items[7], items[8]
    
    def getMF_MT(self, line):
        MF = string_to_float(line[70:72])
        MT = string_to_float(line[72:75])
        return MF, MT
    
    def readLine_string(self):
        items = []
        for i in range(6):
            items.append( self.file.read(11) )  #values
        items.append( self.file.read(4) ) #MAT  
        items.append( self.file.read(2) ) #MF
        items.append( self.file.read(3) ) #MT
        items.append( self.file.read(5) ) #MF-MT line number
        self.file.read(1) #breakline
        
        
        return items
