#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import sys
import os
import subprocess
import zlib

def zlib_compression(global_path, PIGZ_BINARY_PATH, SED_BINARY_PATH):
	#global_path = '/usr/local/share/Geant4-10.4.2/data/G4NDL4.5/'
	
	#data_type = 'ThermalScattering'
	directory = ['Coherent', 'Incoherent', 'Inelastic']
	sub_directory = ['CrossSection', 'FS']
	
	for i in range(len(directory)):
		for j in range(len(sub_directory)):

			path = global_path + '/' + directory[i] + '/' + sub_directory[j] + '/' #data_type + '/' + directory[i] + '/' + sub_directory[j] + '/'
			files = os.listdir(path)

			for k in range(len(files)):
				if 'txt' in files[k]:
					
					#Remove 'nan' in the text file
					file_path_in = path + files[k]
					file_path_in2 = file_path_in
					file_path_in2 = file_path_in2.replace(".txt", "2.txt")
					cmd = SED_BINARY_PATH + " -e 's/nan/''/g' "+file_path_in + ' > '+file_path_in2
					#print('Launch cmd=', cmd)
					process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
					process.communicate() 
					process.wait()
					
					
					#Compress the file
					file_path_out_tmp = path + files[k] 
					file_path_out = file_path_out_tmp.replace(".txt", ".z")
					cmd = PIGZ_BINARY_PATH+" -z1 < "+file_path_in2+" > "+file_path_out
					process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
					process.communicate() 
					process.wait()

					os.remove(file_path_in)
					os.rename(file_path_in2, file_path_in)
					
					os.remove(file_path_in) # Remove .txt file otherwise the final directory is too heavy
