#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import os

#------------------------------------------------------------
#Create directory hierarchy
#------------------------------------------------------------
def createDirectoryHierarchy(file_out_path):
    #if not os.path.exists(file_out_path+'/ThermalScattering'):
    #    os.mkdir(file_out_path+'/ThermalScattering')

    directory_list = ['Coherent', 'Incoherent', 'Inelastic'] #, 'Total', 'Total_elastic', 'Capture']
    subdirectory_list = ['CrossSection', 'FS']
    for directory in directory_list:
        this_directory = file_out_path + '/' + directory #'/ThermalScattering/' + directory
        if not os.path.exists(this_directory):
            os.mkdir(this_directory)

        for subdirectory in subdirectory_list:
            this_subdir = this_directory + '/' + subdirectory
            if not os.path.exists(this_subdir):
                os.mkdir(this_subdir)
    #return file_out_path #+'/ThermalScattering/'
    
