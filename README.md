# Contact and citation
## Contact
 CEA-Saclay  
 DRF/Irfu/DPhN/LEARN  
 91191 Gif-sur-Yvette (FRANCE)  
 Contact: loic.thulliez@cea.fr  

 ## Citation: 
  If you use this code in anyway in your work please use the reference below since substantial efforts went into developping this code:  
		 
-  Published in NIMA:  
		   		L. Thulliez, C. Jouanne and E. Dumonteil,   
				Improvement of Geant4 Neutron-HP package: from methodology to evaluated nuclear data library,  
				
				@article{Thulliez2022,
					author = {L. Thulliez and C. Jouanne and E. Dumonteil},
					doi = {10.1016/J.NIMA.2021.166187},
					issn = {0168-9002},
					journal = {Nuclear Instruments and Methods in Physics Research Section A: Accelerators, Spectrometers, Detectors and Associated Equipment},
					keywords = {Geant4,NJOY processing,SVT method,TRIPOLI-4®,Thermal neutrons,Thermal scattering law},
					month = {3},
					pages = {166187},
					publisher = {North-Holland},
					title = {Improvement of Geant4 Neutron-HP package: From methodology to evaluated nuclear data library},
					volume = {1027},
					year = {2022},
				}
		 
-  ArXiv: 
		    	https://arxiv.org/abs/2109.05967  

# Program installation steps

This program uses python3, so install python3! 

Step1: Installation
-------------------

1.A) Create a directory in which the processing code and all the required data will be, e.g. your_directory_path/tsl_processing_directory/  
		mkdir your_directory_path/tsl_processing_direcory/  
	
1.B) Download the processing code:  
		git clone https://drf-gitlab.cea.fr/lthulli/geant4_tsl_processing.git  
	
1.C) Download NJOY2016:  
		https://github.com/njoy/NJOY2016  
		Download the latest version of the code  
		git clone https://github.com/njoy/NJOY16.git  
		cd NJOY16  
		mkdir bin  
		cd bin  
		cmake -D CMAKE_BUILD_TYPE=Release ..  
		make  
		make test  
	
		Set the njoy executable path in main.py (pwd, etc)  

1.D) Install the pigz software to make the zlib compression of the data for Geant4:  
		sudo apt-get install pigz  
		
	 You will also need the "sed" command from linux to parse some files  
	
1.E) Download the TSL and neutron cross-section files from the preferred evaluated nuclear data library, for example:  
		
- ENDF/B-VII 	->	https://www.nndc.bnl.gov/endf/b7.1/download.html  
- ENDF/B-VIII ->	https://www.nndc.bnl.gov/endf/b8.0/download.html  
- JEFF-3.3	-> 	https://www.oecd-nea.org/dbdata/jeff/jeff33/index.html#neutrons   
	
For consistency we recommend to name the directories as follow:    
- Neutron Reaction Sublibrary 			as 'neutron_the_downloaded_library'   
- Thermal Neutron Scattering Sublibrary 	as 'tsl_the_downloaded_library'  

Therefore now in your your_directory_path/tsl_processing_directory/ you have the following subdirectories: 
 - 'neutron_the_downloaded_library' 
 - 'tsl_the_downloaded_library'  
		

Step2: Setting the program parameters
-------------------------------------

2.A) In the your_directory_path/tsl_processing_directory/geant4_tsl_processing_code you have multiple files named such as:  
	 'files_relation_all_'+files_relation_all_endfB7+'_tsl_'+tsl_the_downloaded_library+'.csv'  
	 It is here to provide the necessary information to NJOY to generate cross-sections and final states.  
	 If you do not find the one that you want to not hesitate to create your own and contact us if you think it is necessary to commit it on the GitLab page.  


2.B) In the your_directory_path/tsl_processing_directory/geant4_tsl_processing_code/main.py in the "User input" section you can define all the relevant parameters to your processing  

Step3: Launch the program
-------------------------

3.A) Go to your_directory_path/tsl_processing_directory/geant4_tsl_processing_code/  

3.B) Execute: python3 main.py  

Basically the program works as follow:  
In launching the script the following actions will be performed:   

1/ The output hierarchy directories will be created by the function 'createDirectoryHierarchy' as:    
TSL_OUTPUT_DIRECTORY (i.e. the one you have specified in main.py):    
- Coherent	  
			     -> CrossSection  
			     -> FS  
- Incoherent	  
		         -> CrossSection  
			     -> FS  
- Inelastic	  
			     -> CrossSection  
				 -> FS  

2/ The TSL processing will be performed with NJOY defined in main.py as NJOY_BINARY_PATH = 'your_njoy_binary_path'
		The NJOYLauncher.py :    
		 	- create symbolic links to the TSL and neutron files to be processed by NJOY, i.e. NEUTRON_XS_DATA_PATH and TSL_DATA_PATH that you specified in main.py    
			- returns the name of the NJOY output file which is 'tape30'.    

3/ Then the file 'tape30' is processed by the module 'NJOYFileProcessor' which convert the NJOY output to Geant4 files and set them into the right directories created at the beginning of the main.py module.    
		This module convert MF=3 (Cross-section as a function of the energy) and MF=6 (FinalState) into Geant4 files.    

4/ The if the material has 'Coherent' elastic scaterring, the FS data are directly extracted from the TSL file under MF=7 and MT=2 identification keys.   

Basically all the parsing module derived from the PENDFFileReader.py module which has the basic functions to parse the PENDFFile.
There is also a Utility.py module to convert float to string, etc.  



