#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import os
import pandas as pd
from Utility import *
from PENDFFileReader import *
from PENDFFile_MF1 import *
from PENDFFile_MF3 import *
from PENDFFile_MF6_inelastic import *
from PENDFFile_MF6_elastic_incoherent import *

class NJOYFileProcessor(PENDFFileReader):
	def __init__(self, njoy_file_in_path, file_out_base, ENDF_LIBRARY, DICT_MT):
		PENDFFileReader.__init__(self,njoy_file_in_path,'dummy')
		self.g4_file_out_list = []

		MF_tmp = 0
		MT_tmp = 0
		temperature_nb = 0
		temperature = 0
		iteration = 0

		MF_MT_pair = []

		line = self.file.readline() #Header containing the file name
		line = self.file.readline()
		isFileReadable = True
		
		while isFileReadable:
			items, MF, MT = self.readLine_and_getMF_MT(line)
			is_line_writtable = True
			
			if( (MF!=3 and MF!=6 and MF!=1 and MF!=2) or MT==0):
				is_line_writtable = False
						
			if MT!=MT_tmp or MF!=MF_tmp:
				if MF_tmp!=0 and MT_tmp!=0:
					self.file_out.close()
					
				if MF_tmp == 1 and MT_tmp!=0: #MT=0 <-> end of MT
					mf1 = PENDFFile_MF1(g4_file_out_path_step1) 
					temperature_nb += 1
					temperature = mf1.get_temperature()
				elif MF_tmp == 3 and MT_tmp!=0: #MT=0 <-> end of MT
					mf3 = PENDFFile_MF3(g4_file_out_path_step1, g4_file_out_path_step2, temperature) # Energy - cross-section
				elif MF_tmp == 6 and MT_tmp!=0: #MT=0 <-> end of MT
					print('Analyze MF6 inelastic, g4_file_out_path_step1=', g4_file_out_path_step1, ' - g4_file_out_path_step2=', g4_file_out_path_step2)
					key = search_key_dictionnary(DICT_MT, MT_tmp)
					print('NJOYFileReader - key=', key)
					if key[2] == 'Inelastic':
						# E - E' - mu
						mf6 = PENDFFile_MF6_inelastic(g4_file_out_path_step1, g4_file_out_path_step2, temperature) 
					elif key[2] == 'Incoherent':
						mf6 = PENDFFile_MF6_elastic_incoherent(g4_file_out_path_step1, g4_file_out_path_step2, temperature) 
					elif key[2] == 'Coherent':
						print('WARNING WARNING -- Coherent -- WARNING WARNING')
				 
				#if MF_tmp > 1 and MT_tmp!=0:
					#print('File path=', g4_file_out_path_step1)
					#os.remove(g4_file_out_path_step1)
				
				if MF!=0 and MT!=0:
					key = search_key_dictionnary(DICT_MT, MT)					
					g4_file_out_path_step1 = file_out_base+'_MF'+str(MF)+'_MT'+str(MT)+'_T'+str(temperature)+'_step1.pendf'
					g4_file_out_path_step2 = file_out_base+'_MF'+str(MF)+'_MT'+str(MT)+'_T'+str(temperature)+'_step2.pendf'
					self.file_out = open(g4_file_out_path_step1, 'w')
					
					if ('Coherent' in key) and (MF==6):
						print('WARNING WARNING -- Coherent FS not dealt with here -- WARNING WARNING')
					else:
						self.g4_file_out_list.append( [int(MF), int(MT), temperature, g4_file_out_path_step2] )
						MF_MT_pair.append((int(MF), int(MT)))
					
			
			if is_line_writtable:
				self.file_out.writelines(line)
			
			MF_tmp = MF
			MT_tmp = MT
			
			line = self.file.readline()
			if line == "":
				isFileReadable = False
			
		#Sort output file by MF and MT to concatenate files with same MF and MT and temperature in descending order
		df = pd.DataFrame(self.g4_file_out_list, columns=['MF', 'MT', 'Temperature', 'File path'])
		df.sort_values(by=['MF', 'MT', 'Temperature'], inplace=True)

		MF_MT_list = list(set(MF_MT_pair)) #Get a unique list independent of temperature
		
		for item in MF_MT_list:
			file_list = df.loc[ (df['MF']==item[0]) & (df['MT']==item[1]), ['File path'] ].values #Get file path
			
			mf_type = search_index_dictionnary(DICT_MF, item[0])
			mt_type = search_index_dictionnary(DICT_MT, item[1])
			
			if len(mf_type)>0 and len(mt_type)>0:
				file_out_g4ndl_path = file_out_base + mt_type[1] + '/' + mf_type[0] + '/' + mt_type[0] +  '.txt'
				file_out_g4ndl = open(file_out_g4ndl_path, 'w')
				file_out_g4ndl.write(formatString('G4NDL') + '\n')
				file_out_g4ndl.write(formatString(ENDF_LIBRARY) + '\n')

				for i in range(len(file_list)):  
					file_in = open( file_list[i][0], 'r')
					lines = file_in.readlines()
					for line in lines:
						line.replace('nan', '')
						file_out_g4ndl.write(line)
					file_in.close()

				file_out_g4ndl.close()

		#Delete temporary files
		for file in os.listdir(file_out_base):
			if file.endswith(".pendf"):
				file_to_be_deleted = os.path.join(file_out_base, file)
				#os.remove( file_to_be_deleted )


		print('--------------------------------------------------')
		print('--------------- Processing summary ---------------')
		print('--------------------------------------------------')
        
