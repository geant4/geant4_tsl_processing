#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import numpy as np
import pandas as pd
from Utility import *
from PENDFFileReader import *


class ENDFFile_MF7_MT2_elastic_coherent(PENDFFileReader):
    def __init__(self, file_in_path, file_out_path, ENDF_LIBRARY):
        print('ENDFFile_MF7_MT2_elastic_coherent: file_in=', file_in_path)
        PENDFFileReader.__init__(self, file_in_path, file_out_path)
        MF = 1
        MT = 451
        
        line = self.file.readline()
        while MF==1 and MT==451:
            line = self.file.readline() #header 0
            MF, MT = self.getMF_MT(line)
            
        while MF!=7 and MT!=2:
            items = self.readLine() #header 0
            MF = items[7]
            MT = items[8]
        
        #Write headers in the output file
        self.file_out.write(formatString('G4NDL')+'\n') 
        self.file_out.write(formatString(ENDF_LIBRARY)+'\n') 
        
        self.ZA = items[0]
        self.AWR = items[1]
        #LTHR = Flag indicating which type of thermal data is being represented.
        #       LTHR=1 for coherent elastic scattering
        self.LTHR = items[2]
        
        items = self.readLine() #header 2
        MF = items[7]
        MT = items[8]
        
        isFirstTemperatureBlock = True
        
        #-------------------------------------------------------------
        # Read and transform PENDF data to G4NDL data
        #-------------------------------------------------------------
        while MF==7 and MT==2:
            temperature = items[0]
            self.LT = items[2]
            braggEdge_nb = 0
            #interpolation = 1.
            
            #This factor is here to know how many point there is to read for a given temperature block
            #The first temperature block gives the Energy_i / Probabilty_i
            #The other tmperature blocks give only Probability_i
            factor = 2. 
            
            if isFirstTemperatureBlock == True:
                self.NR = items[4] #Standard TAB1 interpolation parameters. Use INT=1
                braggEdge_nb = items[5] #Number of Bragg edges given
                #print('first temperature block items=', items)
                items = self.readLine() #header 3
                isFirstTemperatureBlock = False
            else : 
                #print('second temperature block items=', items)
                braggEdge_nb = items[4] #Number of Bragg edges given
                factor = 1. 
               # interpolation = 1 #items[2] - before 2021/10/25
            
            self.file_out.write(formatString(MF)+'\n') 
            self.file_out.write(formatString(MT)+'\n') 
            self.file_out.write(formatString(temperature)+'\n') 
            self.file_out.write(formatString(int(braggEdge_nb))+'\n') 
                        
            line_toRead_number = int(braggEdge_nb*factor/6. +0.9) #int(braggEdge_nb/interpolation*2/6. +0.9) - before 2021/10/25
            data_list = []
            print('line_toRead_number =', line_toRead_number, ' - braggEdge_nb=', braggEdge_nb, ' - factor=', factor, ' - temperature=', temperature)
            #-------------------------------------------------------------
            #Read data block 
            #-------------------------------------------------------------
            line_to_write = ''
            for l in range(line_toRead_number):
                items = self.readLine()
                #print('items=', items)
                data_line = []
                #print('items=', items)
                for i in range(6):  #read pair (energy, cross-section)
                    if items[i]!=None:
                        data_line.append( items[i] ) 
                        #print('items[i]=', items[i])
                        line_to_write = line_to_write + str(format(items[i], '.6e')) + ' '
                
               	
                if l<line_toRead_number:
                	line_to_write = line_to_write + '\n'  
                #print('line_to_write=', line_to_write)              
                self.file_out.write(line_to_write)
                line_to_write = ''
                
                #print('line_to_write=', line_to_write)
                #data_list.append(data_line)  
            #print(data_list)
            #df = pd.DataFrame(data_list, columns=['Energy', 'Cross-section', 'Energy', 'Cross-section', 'Energy', 'Cross-section'])
        
            #values = df.values
            #np.savetxt(self.file_out, values, fmt='%.6e')
            
            #Prepare next data block reading
            items = self.readLine()
            #print('items=', items)
            MF = items[7]
            MT = items[8]
            
        self.file_out.close()

        
