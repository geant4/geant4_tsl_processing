#!/usr/bin/python3

######################################################## 
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import pandas as pd
from Utility import *
from PENDFFileReader import *

class PENDFFile_MF6_inelastic(PENDFFileReader):
    
	def __init__(self, file_in_path, file_out_path, temperature):
		#print('PENDFFile_MF6_inelastic: file_in=', file_in_path)
		PENDFFileReader.__init__(self, file_in_path, 'dummy')
		#-------------------------------------------------------------
		# Read and transform PENDF data to G4NDL data
		#-------------------------------------------------------------        
		items = self.readLine() #header 1
		self.ZA = items[0]
		self.AWR = items[1]
		self.MF = items[7]
		self.MT = items[8]
		items = self.readLine() #header 2
		items = self.readLine() #header 3
		items = self.readLine() #header 4
		#print('In PENDFFile_MF6 Emin-Emax - items=', items)
		self.Emin = items[0]
		self.Emax = items[2]
		items = self.readLine() #header 5
		#print('In PENDFFile_MF6 temperature- items=', items)
		self.temperature = items[0]
		items = self.readLine() #header 6
		#print('PENDFFile_MF6_inelastic: items=', items, ' - file_in_path=', file_in_path)
		if items[0] == None:
			file_out = open(file_out_path, 'w')
			file_out.close()
			return 

		number_pair = int(items[0])
		
		isEOF = False 
		items = self.readLine() #header 7
		
		#---------------------------------------
		# Open output file
		#---------------------------------------
		print('-----------------------------------------')
		print('----------PENDFFile_MF6_inelastic--------')
		print('-----------------------------------------')
		file_out = open(file_out_path, 'w')
		
		file_out.write(formatString(self.MF)+'\n') 
		file_out.write(formatString(self.MT)+'\n') 
		file_out.write(formatString(temperature)+'\n') 
		file_out.write(formatString(number_pair)+'\n') 
		
		file_out.close()
		#file_out = open(file_out_path, 'ab')
		
		while not isEOF:
			E_Ep_mu = []
			data_line = []
			
			#print('In PENDFFile_MF6 energy - items=', items)
			E = items[1]
			data_total_nb = items[4]
			block_size = int(items[5])
			iteration = 0
			block_size_tmp = 0

			#E_Ep_mu.append([0, E, 0, 0, int(data_total_nb), int(block_size), None, None, None, None])
			#print('============= MF6 inelastic ========')
			
			#print('data_total_nb=', data_total_nb)
			while iteration < data_total_nb:
				iteration = iteration + 6
				items = self.readLine()
				#print('iteration=', iteration, ' - data_total_nb=', data_total_nb)
				for j in range(6):
					data_line.append( items[j] )
					block_size_tmp += 1

					if block_size_tmp==block_size:
						block_size_tmp = 0
						E_Ep_mu.append( data_line )
						#print('-> data_line=', data_line)
						data_line = []
			
			#Create a DataFrame
			colunm_names = ['Ep', 'pdf']
			for i in range(block_size-2):
				colunm_names.append('#mu'+str(i))

			df = pd.DataFrame(E_Ep_mu, columns=colunm_names)
			#print('df=', df)
			#input()
			#-------------------------------------------------------------
			# Write GNDL file
			#-------------------------------------------------------------
			#file_in = open(file_out_path+'_tmp', 'r')
			
			#file_out.write('{:<11}'.format('G4NDL')+'\n')
			#file_out.write('{:>11}'.format(ENDF_LIBRARY)+'\n')
						
			data_tmp = [0, E, 0, 0, int(data_total_nb), block_size]
			#print('data_tmp=', data_tmp)
			line = get_line_from_list_without_EOL(data_tmp) + '\n'

			#print('header=', line)

			file_out = open(file_out_path, 'a')
			file_out.write(line)
			#file_out.close()
			#np.savetxt(file_out, data_tmp, fmt='%.6e')
			#Write the data with a numpy array allowing to format the file
			values = df.values
			#print('values=', values)
			
			#values_2 = np.nan_to_num(values, nan=0, posinf=33333333, neginf=33333333)
			np.savetxt(file_out, values, fmt='%.6e')
			file_out.close()
			#input()
			items = self.readLine()
			
			#print('EOF? - items=', items)
			if None in items:
				#print('------- EOF -------')
				isEOF = True
				
		file_out.close()
