#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import sys
import os
import subprocess
import zlib

if __name__ == "__main__":
	global_path = '../ENDF-BVIII/'
	
	data_type = 'ThermalScattering'
	directory = ['Coherent', 'Incoherent', 'Inelastic']
	sub_directory = ['CrossSection', 'FS']
	
	for i in range(len(directory)):
		for j in range(len(sub_directory)):

			path = global_path + data_type + '/' + directory[i] + '/' + sub_directory[j] + '/'
			files = os.listdir(path)
			print('path=', path)
			
			for k in range(len(files)):
				if 'txt' in files[k]:
					file_path_in = path + files[k]
					os.remove(file_path_in) # Remove .txt file otherwise the final directory is too heavy
