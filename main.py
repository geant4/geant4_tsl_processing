#!/usr/bin/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################



import csv
import shutil
from createDirectoryHierarchy import *
from NJOYLauncher import *
from NJOYFileProcessor import *
from ENDFFile_MF7_MT2_elastic_coherent import *
from zlib_compression import *
#from generate_G4ParticleHPThermalScatteringNames import *

if __name__ == "__main__":

	#**************************************************************************
	# User input
	#**************************************************************************
	
	#pigz binary path
	PIGZ_BINARY_PATH = '/usr/bin/pigz'
	
	#sed binary path
	SED_BINARY_PATH = '/bin/sed'
	
	#NJOY binary path
	NJOY_BINARY_PATH = '/local/home/lthullie/Documents/Programs/NJOY/NJOY2016/bin/njoy'
	
	NJOY_EMAX_TSL = '4.0'				#Geant4='4.0' but Tripoli4/MCNP around '5.0'	- string type - for more details have a look at NJOYLauncher.py
	NJOY_ENERGY_TOLERANCE = '0.001' 	#HighPrecision='0.001' 	or LowPrecision='0.02' 	- string type - for more details have a look at NJOYLauncher.py
	NJOY_ANGLE_NUMBER = '32'			#HighPrecision='32' 	or LowPrecision=8' 		- string type - for more details have a look at NJOYLauncher.py
	
	#Path to the repository containing the geant4_tsl_processing program
	DATA_PATH = '/local/home/lthullie/Documents/SONATE/docs/NuclearData_4Geant4/geant4_tsl_processing/'
	
	#Name of the directory containing thermal scattering data (TSL)
	TSL_OUTPUT_DIRECTORY = DATA_PATH + 'ThermalScattering_XS_JEFF33_TSL_ENDFB8_test_20220314/' 
	#'ThermalScattering_XS_JEFF33_TSL_ENDFB8_HighPrecision/' 
	#'ENDF-BVIII' #'JEFF-3.3' #'ENDF-BVIII' #'ENDF-BVIII_mix_with_JEFF33'#'ENDF-BVIII' #'ENDF-BVIII' #'JEFF-3.3'#'ENDF-BVIII' #'ENDF-BVII' 
	
	#Path to neutron cross-section used by NJOY to ensure continuity between cross-section and TSL
	NEUTRON_XS_LIBRARY = 'neutron_xs_jeff33'
	NEUTRON_XS_DATA_PATH = DATA_PATH + NEUTRON_XS_LIBRARY
	
	#Path to TSL data
	TSL_LIBRARY = 'tsl_endfb8' #'tsl_jeff33'
	TSL_DATA_PATH = DATA_PATH + TSL_LIBRARY 
	
	#Path to the file listing all the required informations to process a given TSL 
	FILE_LIST = DATA_PATH + '/geant4_tsl_processing/files_relation_all_jeff33_tsl_endfB8_test_20220314.csv'
	 #files_relation_all_jeff33_tsl_endfB8.csv' #'/geant4_tsl_processing/files_relation_all_jeff33_tsl_endfb8.csv' 
	
	#Path to Geant4 dictionnary for G4ParticleHPThermalScatteringNames.cc class
	GEANT4_DICT_PATH = DATA_PATH + 'geant4_dict.csv' # should be a CSV file
		
	#**************************************************************************
	# The routine starts from here 
	#**************************************************************************
	
	#Create output directory hiearchy
	os.chdir('..')
	cwd = os.getcwd()
	
	#Create the directory where the Thermal scattering data will be stored
	output_data_directory = TSL_OUTPUT_DIRECTORY #cwd +'/'+ TSL_OUTPUT_DIRECTORY + '/'
	if not os.path.exists(output_data_directory):
            os.mkdir(output_data_directory)
	createDirectoryHierarchy(output_data_directory)

	#For each file to process
	csv_file_path = FILE_LIST # cwd +'/'+ TSL_OUTPUT_DIRECTORY+ '/' + FILE_LIST
	csv_file = open(csv_file_path, 'r')
	csv_reader = csv.reader(csv_file, delimiter=",") #names=['TSL', 'Neutron', 'Mtref', 'Natoms', 'Temperature', Geant4Name])
	next(csv_reader, None)
	
	for i, csv_file_row in enumerate(csv_reader):
		print('\n*****************************************************')
		print('******************** Study start '+csv_file_row[0]+' ********************')
		print('*****************************************************')
		print(csv_file_row)
		directory_name = csv_file_row[0].split('.')[0]
		
		outputDirectory_path = TSL_OUTPUT_DIRECTORY + '/' + directory_name + '/' #cwd + '/' + TSL_OUTPUT_DIRECTORY + '/' + directory_name + '/'
		print('outputDirectory_path=', outputDirectory_path)
		if os.path.exists(outputDirectory_path):
		    shutil.rmtree(outputDirectory_path)
		os.mkdir(outputDirectory_path)

		output_file_name = csv_file_row[5]
		     
		njoy_output_file, subDirectory_list = NJOYLauncher(NJOY_BINARY_PATH, NEUTRON_XS_DATA_PATH, TSL_DATA_PATH, NJOY_EMAX_TSL, NJOY_ENERGY_TOLERANCE, NJOY_ANGLE_NUMBER, csv_file_row, outputDirectory_path)
		DICT_MT = []

		#Skip the NJOY step
		#csv_file_row = ['tsl-HinCH2.endf', 'n-001_H_001.endf', '223', '2', '296 350', 'h_polyethylene'] 
		#njoy_output_file = '/local/home/lthullie/Documents/SONATE/docs/NuclearData_4Geant4/ENDF-BVIII/tsl-HinCH2//tape30'
		#subDirectory_list = ['Inelastic', 'Incoherent']

		print('MAIN - njoy_output_file=', njoy_output_file)
		print('MAIN - subDirectory_list=', subDirectory_list)
				
		if len(subDirectory_list)==2:
		    DICT_MT.append( [int(csv_file_row[2]), csv_file_row[5], subDirectory_list[0]] )
		    DICT_MT.append( [int(csv_file_row[2])+1, csv_file_row[5], subDirectory_list[1]] )
		else:
		    DICT_MT.append( [int(csv_file_row[2]), csv_file_row[5], subDirectory_list[0]] )

		#DICT_MT.append( [1, csv_file_row[5], 'Total'] )
		#DICT_MT.append( [2, csv_file_row[5], 'Total_elastic'] )
		#DICT_MT.append( [102, csv_file_row[5], 'Capture'] )
		print('DICT_MT=', DICT_MT)    
		
		#------------------------------------------------------
		# From NJOY to G4NDL
		#------------------------------------------------------

		#njoy_output_file = '/local/home/lthullie/Documents/SONATE/docs/NuclearData_4Geant4/ENDF-BVII/CH2/tape30'

		#Process all inelastic and incoherent cross-section
		#njoy_process = NJOYFileProcessor('/local/home/lthullie/Documents/SONATE/docs/NuclearData_4Geant4/CH2/ENDF-BVII/CH2/tape31', 'ThermalScattering/')
		#njoy_process = NJOYFileProcessor('/local/home/lthullie/Documents/SONATE/docs/NuclearData_4Geant4/ENDF-BVIII/CH2/tape31', 'ThermalScattering/')
		#njoy_process = NJOYFileProcessor('/local/home/lthullie/Documents/SONATE/docs/NuclearData_4Geant4/Vaibhav/tape31', 'ThermalScattering/')
		
		#Analyze cross-section, incoherent and inelastic FS
		
		print('njoy_output_file=', njoy_output_file)
		print('output_data_directory=', output_data_directory)
		print('DICT_MT=', DICT_MT)
		

		njoy_process = NJOYFileProcessor(njoy_output_file, output_data_directory, TSL_OUTPUT_DIRECTORY, DICT_MT)
		
		#Analyze coherent FS
		if 'Coherent' in subDirectory_list:
		    file_in_path = TSL_DATA_PATH + '/' + csv_file_row[0]   #cwd + '/' + TSL_OUTPUT_DIRECTORY + '/tsl/' + csv_file_row[0]
		    file_out_path = output_data_directory + '/Coherent/FS/' + output_file_name + '.txt'
		    print('COHERENT: file_in_path=', file_in_path)
		    print('COHERENT: file_out_path=', file_out_path)
		    coherent_process = ENDFFile_MF7_MT2_elastic_coherent(file_in_path, file_out_path, TSL_OUTPUT_DIRECTORY)


		shutil.rmtree(outputDirectory_path)
		
		#----------------------------------------------------
		# Data compression
		#----------------------------------------------------
		zlib_compression(output_data_directory, PIGZ_BINARY_PATH, SED_BINARY_PATH) #cwd + '/' + TSL_OUTPUT_DIRECTORY + '/' )
		
		file_list = os.listdir(output_data_directory) #cwd + '/' + TSL_OUTPUT_DIRECTORY) # + '/ThermalScattering/')
		for file in file_list:
			if '.pendf' in file:
				os.remove(output_data_directory + '/' + file) #cwd + '/' + TSL_OUTPUT_DIRECTORY) # + '/ThermalScattering/' + file)
				
		#----------------------------------------------------
		# Add this element do the Geant4 dictionnary
		#----------------------------------------------------
		#To do in a separate step - more efficient
		#generate_G4ParticleHPThermalScatteringNames(GEANT4_DICT_PATH, TSL_LIBRARY, csv_file_row[5], csv_file_row[6])
		
		 
	#Data compression
	#zlib_compression(cwd + '/' + TSL_OUTPUT_DIRECTORY + '/' )
	#file_list = os.listdir(cwd + '/' + TSL_OUTPUT_DIRECTORY + '/ThermalScattering/')
	#for file in file_list:
	#	if '.pendf' in file:
	#		os.remove(cwd + '/' + TSL_OUTPUT_DIRECTORY + '/ThermalScattering/' + file)
	
	   
	print('\n*******************************************')
	print('********* End of data processing **********') 
	print('*******************************************')  
