#!/usr/bon/python3

########################################################
# CEA-Saclay
# DRF/Irfu/DPhN/LEARN
# 91191 Gif-sur-Yvette (FRANCE)
# Contact: loic.thulliez@cea.fr
########################################################

import os
import sys
import re
import json
import pandas as pd
import numpy as np
import subprocess

def string_to_float(string): 
	string_out_tmp = string.replace(' ', '')
	if string_out_tmp != '':
		string_out = string_out_tmp
		if (not ('E' in string_out_tmp)) and (not ('e' in string_out_tmp)):
			string_out = string_out_tmp.replace('+','E+')
		
		splits = string_out.split('.')
		if len(splits) > 1:
			splits_2 = splits[1]
			if (not ('E' in splits[1])) and (not ('e' in splits[1])):
				splits_2 = splits[1].replace('-','E-')
			string_out = splits[0] + '.' + splits_2
		
		return float(string_out)
    
def string_to_int(string): 
    string_out_tmp = string.replace(' ', '')
    if string_out_tmp != '':
        string_out = string_out_tmp.replace('+','E+')
        splits = string_out.split('.')
        if len(splits) > 1:
            splits_2 = splits[1].replace('-','E-')
            string_out = splits[0] + '.' + splits_2
        
        return int(string_out)
     
#def get_line_from_list(items):
#    line = ''
#    for i in range(6):
#        line += '{:>11}'.format(items[i]) #values
#    line += '{:>4}'.format( int(items[6]) ) #MAT  
#    line += '{:>2}'.format( int(items[7]) ) #MF
#    line += '{:>3}'.format( int(items[8]) ) #MT
#    line += '{:>5}'.format( int(items[9]) ) #MF-MT line number
#    return line

def formatString(item):
	string = ''
	if item!=None: # and type(item)!=str:
		string = '{:>13}'.format(item) # '{:>11}'.format(item)
	#elif item!=None:
	#	string = '{:>13}'.format(item)
	return string

def get_line_from_list(items):
    line = ''
    for item in items:
        line += formatString(item) #values

def get_line_from_list_without_EOL(items):
    line = ''
    for item in items:
        line += formatString(item) #values
    return line

def get_line_from_list_6char_without_EOL(items):
    line = ''
    for i in range(6):
        line += formatString(items[i]) #values
    return line

def get_line_from_list_with_EOL(items):
    line = get_line_from_list_without_EOL(items)
    line += '\n'
    return line


def search_index_dictionnary(dictionnary, index_to_find):
    data = []
    
    for i in range(len(dictionnary)):
        if dictionnary[i][0] == index_to_find :
            for j in range( len(dictionnary[i])-1 ): #offset of 1 because index 0 is the index to match
                data.append(dictionnary[i][j+1])     #offset of 1 because index 0 is the index to match
            break
    return data

def search_key_dictionnary(dictionnary, key):
    data = []
    
    for i in range(len(dictionnary)):
            for j in range( len(dictionnary[i]) ): #offset of 1 because index 0 is the index to match
                if dictionnary[i][j] == key:
                    data = dictionnary[i]
                    break
    return data


